//
//  JSONManager.swift
//  SystemTask
//
//  Created by Sreenadh Ginjupalli on 18/06/20.
//  Copyright © 2020 Malinayudu. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class JSONManager: NSObject {
    
    static let shared = JSONManager()

    
    func getCountryDetailList(data: [[String:Any]]) -> [countryDetailModel] {
        var arrData = [countryDetailModel]()
        data.forEach { (item) in
            arrData.append(countryDetailModel(data: item))
        }
        return arrData
    }
    
  
}


