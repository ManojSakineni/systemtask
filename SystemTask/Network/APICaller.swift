//
//  APICaller.swift
//  SystemTask
//
//  Created by Sreenadh Ginjupalli on 18/06/20.
//  Copyright © 2020 Malinayudu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class APICaller {
    
    static let shared = APICaller()

    func serialize(json: JSON) -> String {
        return json.rawString() ?? "".replacingOccurrences(of: "\\/", with: "/")
    }
   
    let headers = ["content-Type" : "text/plain",
                   "Accept"       : "text/plain" ] as HTTPHeaders

    
    let ownError = NSError(domain: "",
                                  code: 0,
                                  userInfo: [ NSLocalizedDescriptionKey: "unknown error from server"]) as Error

}
