//
//  Service.swift
//  SystemTask
//
//  Created by Sreenadh Ginjupalli on 18/06/20.
//  Copyright © 2020 Malinayudu. All rights reserved.
//

import UIKit
import Alamofire
import CommonCrypto
import SwiftyJSON

extension APICaller {
    
    // MARK: - Post Request Method.
    func postRequest(_ url: String, _ params: [String: Any], completion:@escaping (_ result:[String:Any]?, _ success:Bool) -> Void) {
        
        let urlString = baseURL
        AF.request(urlString,
                   method: .post,
                   parameters:params,
                   encoding: JSONEncoding.default,
                   headers:headers)
            .validate()
            .responseString(completionHandler: { response in
                switch response.result {
                case .success:
                    if let data = response.value {
                        print(data.data(using: String.Encoding.utf8)! )
                        if let data = data.data(using: String.Encoding.utf8) {
                            
                            do {
                                let dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                                
                                if let myDictionary = dictonary
                                {
                                    print("Response is \(myDictionary["title"] ?? "n/a")")
                                    completion(myDictionary, true)
                                } else {
                                    completion(nil, false)
                                }
                                
                            } catch let error as NSError {
                                print(error)
                                completion(nil, false)
                            }
                        }
                        
                    }
                case .failure(let error):
                    print(error)
                    completion(nil, false)
                }
            })
        
    }
}
