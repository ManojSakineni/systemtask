//
//  CountryDetailCell.swift
//  SystemTask
//
//  Created by Sreenadh Ginjupalli on 19/06/20.
//  Copyright © 2020 Malinayudu. All rights reserved.
//

import UIKit

class CountryDetailCell: UITableViewCell {
    var countryDetails:countryDetailModel? { /// Country Details Data SetUp
        didSet {
            guard let detailItem = countryDetails else {return}
            if let name = detailItem.title {
                self.titleLabel.text = name
            } else {
                self.titleLabel.text = ""
            }
            
            if let description = detailItem.description {
                detailedLabel.text = description
            } else {
                self.detailedLabel.text = ""
            }
            if let imageURL = detailItem.imageName {
                self.detailImageView.setImage(imageURL, "photo.fill")
            }
        }
    }
    override func prepareForReuse() {
           super.prepareForReuse()
           detailImageView.image = UIImage(systemName: "photo.fill")
       }

       // Called in cellForRowAt / cellForItemAt
    let detailImageView:UIImageView = { ///Image VIew
        let img = UIImageView()
        img.contentMode = .scaleAspectFit // image will never be strecthed vertially or horizontally
        img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
        img.image = UIImage(systemName: "photo.fill")
        img.tintColor = .lightGray
        img.clipsToBounds = true
        return img
    }()
       
    let titleLabel:UILabel = { ///Title
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        return label
    }()
       
    let detailedLabel:UILabel = { /// Description
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor =  .black
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
       
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.backgroundColor = .white
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(detailedLabel)
        self.contentView.addSubview(detailImageView)
        
        self.backgroundColor = UIColor.white
        
        
        /// Assigning Constraints
        NSLayoutConstraint.activate([
            
            detailImageView.topAnchor.constraint(equalTo:self.contentView.topAnchor,constant: 10.0),
            detailImageView.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor,constant: 10.0),
            detailImageView.trailingAnchor.constraint(equalTo:self.contentView.trailingAnchor, constant: -10.0),
            detailImageView.heightAnchor.constraint(equalToConstant: 200.0),
    
            titleLabel.topAnchor.constraint(equalTo:self.detailImageView.bottomAnchor,constant: 5),
            titleLabel.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor,constant: 10.0),
            titleLabel.trailingAnchor.constraint(equalTo:self.contentView.trailingAnchor, constant: -10.0),

            detailedLabel.topAnchor.constraint(equalTo:self.titleLabel.bottomAnchor, constant: 10),
            detailedLabel.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor,constant: 10.0),
            detailedLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10.0),
            detailedLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -10.0),
        ])
        
    }

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }

}
