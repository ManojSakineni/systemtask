//
//  CountryDetailViewController.swift
//  SystemTask
//
//  Created by Sreenadh Ginjupalli on 19/06/20.
//  Copyright © 2020 Malinayudu. All rights reserved.
//

import UIKit
import Reachability

class CountryDetailViewController: UIViewController {
    let reachability = try! Reachability()
    
    // MARK: - Constanta and Variables
    let refreshControl = UIRefreshControl()
    private let detailCellIdentifier = "detailCellIdentifier"
    let countryDetailTableView = UITableView()
    let countryDetailViewModel = CountryDetailViewModel()
    var arrData = [countryDetailModel]()
    var headerTitle = "Country"
    var isRefreshing = false
    
    
    // MARK: - Viewlife Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addRefreshController()
        initialSetUp()
        self.tableViewSetUp()
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
    }
    // MARK: - Cuustom Methods
    @objc func reachabilityChanged(note: Notification) { // rechability change status mehtod
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .wifi,.cellular:
            self.getCountryData()
        case .unavailable:
            networkAlertMethod()
        case .none:
            print("")
        }
    }
    func initialSetUp() { /// Initial Set Up
        self.navigationItem.title = headerTitle
        self.countryDetailTableView.delegate = self
        self.countryDetailTableView.dataSource = self
        DispatchQueue.main.async {
            self.countryDetailTableView.reloadData()
        }
    }
    func addRefreshController() {/// Adding Pull to refresh contoller
        // Add Refresh Control to Table View
        self.countryDetailTableView.addSubview(refreshControl)
        refreshControl.tintColor =  UIColor(named: "AppColor")
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) { /// Handlin Refresh Action
        self.isRefreshing = true
        self.getCountryData()
    }
    
    func tableViewSetUp() { /// Tableview SetUp
        self.view.backgroundColor = .white
        self.countryDetailTableView.backgroundColor = .white
        self.countryDetailTableView.translatesAutoresizingMaskIntoConstraints = false
        self.countryDetailTableView.tableFooterView = UIView()
        self.countryDetailTableView.register(CountryDetailCell.self, forCellReuseIdentifier: "CountryDetailCell")
        self.view.addSubview(self.countryDetailTableView)
        
        NSLayoutConstraint.activate([
            self.countryDetailTableView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
            self.countryDetailTableView.leftAnchor.constraint(equalTo:view.safeAreaLayoutGuide.leftAnchor),
            self.countryDetailTableView.rightAnchor.constraint(equalTo:view.safeAreaLayoutGuide.rightAnchor),
            self.countryDetailTableView.bottomAnchor.constraint(equalTo:view.safeAreaLayoutGuide.bottomAnchor)
        ])
        
    }
    
    func getCountryData() { /// API Calling........
        APICaller.shared.postRequest("", [:]) { (data, success) in
            if success {
                guard let responseData = data   else { return }
                
                if let value = responseData["title"] as? String {
                    self.headerTitle = value
                }
                if let detailArray = responseData["rows"] as? [[String:Any]] {
                    self.arrData = JSONManager.shared.getCountryDetailList(data: detailArray)
                    print("Detail Count is \(self.arrData)")
                    self.navigationItem.title = self.headerTitle
                    self.countryDetailTableView.reloadData()
                    self.endRefreshControlMethod()
                }
            } else {
                self.networkAlertMethod()
            }
        }
    }
    func endRefreshControlMethod(){ /// End refresh controll
        if self.isRefreshing == true {
            self.isRefreshing = false
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
        }
    }
    func networkAlertMethod(){ // No internet Alert
        let alertView = UIAlertController(title: "Please check your internet connection.", message: "", preferredStyle:.alert)
        alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction ) in
            // Put some code for okay button
            self.endRefreshControlMethod()
        }))
        self.present(alertView, animated: true, completion: nil)
    }
}

extension CountryDetailViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableview Delegate and Datasource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrData.isEmpty {
            self.setEmptyMessage("No Data Available")
        } else {
            self.restore()
        }
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryDetailCell", for: indexPath) as! CountryDetailCell
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        cell.countryDetails = arrData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
extension CountryDetailViewController{
    func setEmptyMessage(_ message: String) {
        let emptyView = UIView(frame: CGRect(x: 0, y: 0, width: self.countryDetailTableView.bounds.size.width, height: self.countryDetailTableView.bounds.size.height))
        emptyView.backgroundColor = .white
        let messageLabel: UILabel = UILabel(frame: CGRect(x: 37, y: 350, width: self.countryDetailTableView.bounds.size.width-74, height: 200))
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        messageLabel.center.x = emptyView.center.x
        messageLabel.center.y = emptyView.center.y
        emptyView.addSubview(messageLabel)
        self.countryDetailTableView.backgroundView = emptyView
    }
    func restore() {
        self.countryDetailTableView.backgroundView = nil
    }
}
