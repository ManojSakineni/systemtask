//
//  Extention.swift
//  SystemTask
//
//  Created by Sreenadh Ginjupalli on 19/06/20.
//  Copyright © 2020 Malinayudu. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    func setImage(_ url: String) { /// Image loading with URL
        self.kf.setImage(with: URL(string: url))
    }
    func setImage(_ url: String, _ placeHolder: String) {
        print("Image url is \(URL(string: url))")
        self.kf.setImage(with: URL(string: url), placeholder: UIImage(systemName:placeHolder))
    }
}
