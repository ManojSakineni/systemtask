//
//  CountryDetailModel.swift
//  SystemTask
//
//  Created by Sreenadh Ginjupalli on 18/06/20.
//  Copyright © 2020 Malinayudu. All rights reserved.
//

import Foundation
import SwiftyJSON

struct countryDetailModel { /// Country Detail Model
    
    public var title: String?
    public var description: String?
    public var imageName: String?
    
    init(data:[String:Any]) {
        if let value = data["title"] as? String {
            self.title = value
        }
        
        if let value = data["description"] as? String {
            self.description = value
        }
        
        if let value = data["imageHref"] as? String {
            self.imageName = value
        }
    }
}
